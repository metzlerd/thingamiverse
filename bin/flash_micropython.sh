#!/usr/bin/env bash
file="$1"
if [[ -z "$1" ]]; then
  file=~/dev/esptool/esp8266-20170823-v1.9.2.bin
fi
esptool.py --port /dev/tty.usbserial-14210 erase_flash
esptool.py --port /dev/tty.usbserial-14210 --baud 115200 write_flash --flash_size=detect -fm dio 0 $file