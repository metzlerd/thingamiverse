#!/usr/bin/env bash
client_dir="python/client"
port=""
if [[ -e "/dev/ttyUSB0" ]] ; then
  port="/dev/ttyUSB0"
fi
if [[ -e "/dev/tty.wchusbserialfd130" ]] ; then
  port="/dev/tty.wchusbserialfd130"
fi
if [[ -e "/dev/tty.usbserial-144110" ]] ; then
  port="/dev/tty.usbserial-144110"
fi
if [[ -e "/dev/tty.usbserial-14210" ]] ; then
  port="/dev/tty.usbserial-14210"
fi

if [[ -z "$port" ]]; then
  echo "No port found";
  exit 1
fi
## type of thing is first paramter
type="$1"
## settings to copy from is the second parameter;
## if not specified use the first parameter for both settings and type.
if [[ -z "$2" ]] ; then
  settings="$1"
else
  settings="$2"
fi
temp_dir="$HOME/staging"
echo "Copying files to staging..."
cp $client_dir/$type.py $temp_dir/main.py
cp $client_dir/$settings.json $temp_dir/settings.json
cp $client_dir/thing.py $temp_dir/thing.py
cp $client_dir/neopixelator.py $temp_dir/neopixelator.py
cd $temp_dir
echo "Programming device"
echo "Settings"
ampy -p $port put settings.json
sleep 5
echo "Thing"
ampy -p $port put thing.py
sleep 5
echo "Neopixelator"
ampy -p $port put neopixelator.py
sleep 5
echo "main"
ampy -p $port put main.py
sleep 5
ampy -p $port ls
echo "Cleaning Up"
rm -f $temp_dir/settings.json
rm -f $temp_dir/*.py
