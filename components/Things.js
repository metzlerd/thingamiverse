
Things = {

  thingList :{},

  init: function() {
    this.checkStatus();
    setInterval(this.checkStatus, 5000);
    this.processAjaxClasses();
  },

  checkStatus : function () {
    Things.send('/things/status');
  }
  ,

  updateStatus : function (status) {
    console.log(status.things);
    console.log(this.thingList);
    for (var d in this.thingList) {
    console.log(d);
      console.log(d);
      if (!d in status.things) {
        console.log(d + " not found");
        jQuery('#thing-'+d).remove()
      }
      else {
        console.log(d + " found");
      }
    }
    this.thingList = status.things;
  },

  /**
   * Send an ajax request to a url.
   * @param url
   */
  send: function(url) {
    jQuery.ajax(
      url,
      {
        dataFormat : "json",
        success: this.handleResponse,
        error: this.handleError
      }
    );
  },

  handleError: function(xhr, ajaxOptions, thrownError) {
    console.log('Error:');
    console.log(xhr);
    console.log(thrownError);
  },

  /**
   * Process teh results of an ajax response.
   * @param response
   */
  handleResponse: function (response) {
    // Check for jquery functions.
    // console.log(response);
    if (response.jQuery) for (var i=0; i < response.jQuery.length; i++) {
      Things.jQueryInvoke(response.jQuery[i]);
    }
    Things.processAjaxClasses();
    jscolor.installByClassName('jscolor');
  },

  /**
   * Invoke a jquery command passed from the ajax reponse
   * @param command
   *   Object describing the jquty command that needs to be run.
   *  { "selector": { command: [parm1, parm2, parm3]} }
   */
  jQueryInvoke: function(command) {
    for (var selector in command) {
      if (command.hasOwnProperty(selector)) {
        context = jQuery(selector);
        method = command[selector];

        for (var fn in method) {
          if (method.hasOwnProperty(fn)) {
            args = method[fn];
            callable = context[fn];
            if (typeof callable === "function"){
              context[fn].apply(context, [args]);
            }
          }
        }
      }
    }
  },

  ajaxLinkClick: function(e) {
    e.preventDefault();
    href = jQuery(this).attr('href');
    Things.send(href);
  },

  ajaxSubmitClick: function (e) {
    e.preventDefault();
    url = this.form.getAttribute("action");
    action = this.getAttribute("name");
    url = url + "/" + action;
    console.log(url);
    jQuery.ajax(
      url,
      {
        type: "POST",
        data: jQuery(this.form).serialize(),
        dataFormat : "json",
        success: Things.handleResponse,
        error: Things.handleError
      }
    );
  },

  processAjaxClasses: function () {
    jQuery('.use-ajax:not(.ajax-processed)').click(Things.ajaxLinkClick);
    jQuery('.use-ajax:not(.ajax-processed)').addClass('ajax-processed');
    jQuery('input[type="submit"]:not(.ajax-processed)').click(Things.ajaxSubmitClick);
    jQuery('input[type="submit"]:not(.ajax-processed)').addClass('ajax-processed');
  }
};

// Initialize all the things
jQuery(document).ready(Things.init());
jQuery.fn.thingStatus = function(data) {
  Things.updateStatus(data);
}

