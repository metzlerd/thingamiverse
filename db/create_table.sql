
  create table messages (
    mid SERIAL PRIMARY KEY,
    topic VARCHAR(40),
    action CHAR(1) DEFAULT 'S',
    data VARCHAR(1000),
    recorded TIMESTAMP not null default CURRENT_TIMESTAMP
  );
  CREATE INDEX message_recorded_idx ON messages (recorded);
  CREATE INDEX topic_idx ON messages (topic);
