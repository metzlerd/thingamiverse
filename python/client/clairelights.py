import neopixelator
from thing import Thing
import time
import sys

t = Thing()
pin = 4
leds = 15

#configure inital lights
if "pin" in t.config.keys():
    pin = t.config["pin"]
if "leds" in t.config.keys():
    leds = t.config["leds"]
lights = neopixelator.Lights(pin,leds)

#Set initial state
state = dict(t.config["lights"])

#define the functions
def action():
    global state
    if state["effect"] == "down":
        lights.rotate_down()
        lights.write()

def lights_on(data):
    global state
    if "effect" in data.keys():
        state["effect"] = data["effect"]
    if "fill" in data.keys():
        state["fill"] = data["fill"]
    if "stripe" in data.keys():
        state["stripe"] = data["stripe"]
    if "interval" in data.keys():
        state["interval"] = data["interval"]
    colors = neopixelator.hex_colors(state["stripe"])
    if state["fill"] == "spread":
        lights.spread_colors(colors)
    else: 
        lights.fill_colors(colors)
    lights.write()
    
def lights_off(data):
    global lights
    lights.fill_color(0,0,0)
    lights.write()

t.set_action("lights", lights_on)
t.set_action("off",lights_off)
t.connect()
lights_on(state)
try:    
    while 1:
        t.check_msg()
        time.sleep(state["interval"])
        action()
except:
    print("Unexpected error:")
    t.disconnect()
    raise
