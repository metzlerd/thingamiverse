import machine
import json
import time
from umqtt.robust import MQTTClient

c = MQTTClient("led_simple", "192.168.1.100")
pin = machine.Pin(16, machine.Pin.OUT)

# Define a callback that parses json and sets a pin
def led_callback(topic, msg):
    m = json.loads(msg)
    pin.value(m['value'])

c.set_callback(led_callback)
time.sleep(5)
c.connect(clean_session=True)
c.subscribe("led")
while 1:
    c.wait_msg()
c.disconnect()
