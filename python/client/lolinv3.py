from thing import Thing

def debug(message):
    print('action:', message)

t = Thing()
t.set_action('led', debug)
t.connect()
while 1:
    t.wait_msg()
