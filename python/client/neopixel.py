# A mock neopixel module for debugging mircopython code.
import json
class NeoPixel(list):
    pin=0
    depth=0
    buf=None
    
    def __init__(self, pin, depth):
        self.pin = pin
        self.depth = depth
        for i in range(0,depth):
            self.append((0,0,0))
        self.update_buffer()

    def __setitem__(self, key, value):
        super(NeoPixel, self).__setitem__(key, value)
        self.update_buffer()
            
    def update_buffer(self):
        l = []; 
        for i in range(0,self.depth):
            c=list(self[i])
            l.append(c[0])
            l.append(c[1])
            l.append(c[2])
        self.buf=bytearray(l)        
  
    def write(self):          
        print("pin:", self.pin)
        print(self.buf)
        
        
