import machine
import time
import neopixel
from umqtt.robust import MQTTClient
try:
    import ubinascii
except:
    import binascii as ubinascii

def hex_colors(stripe):
    l=[]
    try:
        b = ubinascii.unhexlify(stripe)
        for i in range(0,len(stripe)//6):
            l.append((b[i*3], b[i*3+1], b[i*3+2]))
    except:
        print("invalid hex colors")
    return l


class Lights:
    size=15
    pin=15
    def __init__(self, pin, size):
        self.pin=pin
        self.size=size
        self.neopixel = neopixel.NeoPixel(machine.Pin(pin), size)

    # Fill all pixels with a color
    def fill_color(self, red, green, blue):       
        for i in range(0, self.size):
            self.neopixel[i] = (red, green, blue)

    def fill_colors(self, colors):
        s = self.size if self.size >= len(colors) else len(colors)
        if len(colors)==0:
            return
        for i in range(0,s//len(colors)):
            for j in range(0, len(colors)):
                if j+i*len(colors) < self.size:
                    self.neopixel[i*len(colors)+j] = colors[j]

    def spread_colors(self, colors):
        # Skip if we have no colors
        if len(colors)==0:
            return
        s = self.size if self.size >= len(colors) else len(colors)
        n = s//len(colors)
        cl = len(colors)
        x=0
        for j in range(0, cl):
            for i in range(0, n):
                if x < self.size:
                    f = 1-abs(i+0.5-n/2)/(n/2)
                    c = list(colors[j])
                    c = (int(c[0]*f), int(c[1]*f),int(c[2]*f))

                    self.neopixel[x] = c
                x=x+1

    def rotate_up(self, count=1):
        px = list(self.neopixel.buf)
        if len(px) > 3:
            for i in range(0,count):
                r = px.pop(0)
                g = px.pop(0)
                b = px.pop(0)
                px.append(r)
                px.append(g)
                px.append(b)
        self.neopixel.buf = bytearray(px)

    def rotate_down(self, count=1):
        px = list(self.neopixel.buf)
        if len(px) > 3:
            for i in range(0,count):
                b = px.pop(len(px)-1)
                g = px.pop(len(px)-1)
                r = px.pop(len(px)-1)
                px.insert(0,b)
                px.insert(0,g)
                px.insert(0,r)
        self.neopixel.buf = bytearray(px)

    def write(self):
        self.neopixel.write()
    
                   
    # Neopixel library.            
    def process_neopixel(msg):
        count = msg['count']
        delay = msg['delay']
        stripes = msg['stripes']
        for x in range(0,count): 
            for stripe in stripes:
                pin = stripe['pin']
                pixels = stripe['pixels']
                depth = len(pixels)
                d = stripe['delay']
                i=0
                for pixel in pixels:
                    p[i] = tuple(pixel)
                    i=i+1
                p.write()
                if d > 0:
                    time.sleep(d/1000)
            if delay > 0:
                time.sleep(delay/1000)
   
