# Mock module for network behaviors
STA_IF=1
class wlan:
    def active(self, status):
        return True
    def isconnected(self):
        return True
    def ifconfig(self):
        return "connection"
    
def WLAN(type):
    return wlan()
