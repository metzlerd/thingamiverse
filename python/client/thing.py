import json
import time
import os
import network
from umqtt.robust import MQTTClient

class Thing:
    cient = None
    name = "lolinv3"
    essid = ""
    wifi_password = ""
    type = "thing"
    server = "192.168.1.100"
    callbacks = {}
    message = None
    config = {}
    check_time = 0

    # Define basic callback
    def __init__(self):
        #Attempt to load json file
        check_time = time.time()
        with open('settings.json') as json_data:
            config = json.load(json_data)
            self.config = config
            if 'server' in config.keys():
                self.server = config["server"]
            if 'name' in config.keys():
                self.name = config["name"]
            if 'type' in config.keys():
                self.type = config["type"]
            if 'essid' in config.keys():
                self.essid = config["essid"]
            if 'wifi_password' in config.keys():
                self.wifi_password = config["wifi_password"]

        self.client = MQTTClient(self.name, self.server, 0, None, None, 10)
        self.client.set_callback(self.message_callback)

    def set_action(self, action, callback):
        self.callbacks[action] = callback

    # Define a callback that parses json and sets a pin
    def message_callback(self, topic, msg):
        topic=topic.decode('utf-8')
        print(topic)
        parts = topic.split('/')
        action = parts[-1]
        m = json.loads(msg.decode('utf-8'))
        if action in self.callbacks:
            c = self.callbacks[action]
            c(m)

    def connect(self):
        self.wifi_connect(self.essid, self.wifi_password)
        time.sleep(5)
        m = {}
        m['name'] = self.name
        m['type'] = self.type
        d = json.dumps(m)
        self.client.set_last_will('thing/leave',d)
        self.client.connect(clean_session=True)
        self.client.set_last_will('thing/leave',d)
        self.client.set_callback(self.message_callback)
        self.client.publish('thing/join',d)
        self.client.subscribe(self.name + "/#")

    def wait_msg(self):
        self.client.wait_msg()

    def check_msg(self):
        t = time.time()
        if t - self.check_time > 7:
            self.check_time = t
            self.client.ping()
        self.client.check_msg()

    def disconnect(self):
        m = {}
        m['name'] = self.name
        m['type'] = self.type
        d = json.dumps(m)
        self.client.publish('thing/leave',d)
        self.client.disconnect()

    def wifi_connect(self, essid, password):
        # Connect to the wifi. Based on the example in the micropython
        # documentation.
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        if not wlan.isconnected():
            print('connecting to network ' + essid + '...')
            wlan.connect(essid, password)
            # connect() appears to be async - waiting for it to complete
            while not wlan.isconnected():
                print('waiting for connection...')
                time.sleep(4)
                print('checking connection...')
            print('Wifi connect successful, network config: %s' % repr(wlan.ifconfig()))
        else:
            # Note that connection info is stored in non-volatile memory. If
            # you are connected to the wrong network, do an explicity disconnect()
            # and then reconnect.
            print('Wifi already connected, network config: %s' % repr(wlan.ifconfig()))

    def wifi_disconnect(self):
        # Disconnect from the current network. You may have to
        # do this explicitly if you switch networks, as the params are stored
        # in non-volatile memory.
        wlan = network.WLAN(network.STA_IF)
        if wlan.isconnected():
            print("Disconnecting...")
            wlan.disconnect()
        else:
            print("Wifi not connected.")

    def disable_wifi_ap(self):
        # Disable the built-in access point.
        wlan = network.WLAN(network.AP_IF)
        wlan.active(False)
        print('Disabled access point, network status is %s' %
              wlan.status())
