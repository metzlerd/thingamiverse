import time
import os
import paho.mqtt.client as mqtt
class MQTTClient:
    name = ''
    host = ''
    topic = ''
    client = {}
    stopped = False
    
    def __init__(self, name, host, port=0, user=None, password=None, keepalive=0):
        self.name = name
        self.host = host
        self.client = mqtt.Client(name)

    def on_connect(self, mqtt, userdata, flags, rc):
        mqtt.subscribe(self.topic)
        print("Subscribing to:", self.topic)

    def on_message(self, mqtt, userdata, message):
        topic = message.topic.encode('utf-8')
        message = message.payload
        print('topic: ',topic)
        self.callback(topic, message)
        
    def set_callback(self, callback):
        self.callback = callback

    def connect(self, clean_session):
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        print('connecting to host', self.host)
        self.client.connect(self.host)
        self.clean_session = clean_session

    def disconnect(self):
        self.client.disconnect()

    def ping(self):
        x=1

    def set_last_will(self, topic, message=None, qos=0, retain=False):
        self.client.will_set(topic, message)

    def publish(self, topic, message):
        self.client.publish(topic, message)
        print(topic, ':', message)
        
    def subscribe(self, topic):
        self.topic = topic

    def wait_msg(self):
        self.client.loop_forever()

    def check_msg(self):
        if self.stopped:
            self.client.loop_start()
        self.client.loop()
        

        
