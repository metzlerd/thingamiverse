import paho.mqtt.client as mqtt
import os
import json
import service

project_path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
path =  project_path + '/config'
print(path)
with open(path+'/config.json') as config_file: 
  config = json.load(config_file)
ds = service.DataService(config, 'pi_writer', project_path)
ds.listen()
