import paho.mqtt.client as mqtt
import time
import os
import json
import io

class DataService:
    db = {}
    client = {}
    run=True
    name=''
    temp_dir='/var/temp'

    status = {"things" : {}}

    def __init__(self, config, name, data_path):
        self.client = mqtt.Client()
        self.name=name
        self.mqtt_connect(config["mqtt"])
        self.temp_dir = data_path + '/things'

    def save_status(self):
        print("saving to " + self.temp_dir)
        import sys
        if sys.version_info[0] >= 3:
          unicode = str
        with io.open(self.temp_dir + '/things.json', 'w', encoding='utf-8') as f:
            f.write(unicode(json.dumps(self.status)))
        f.close()

    def mqtt_connect(self, config):
        self.client = mqtt.Client(self.name)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.connect(config["host"])

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result")
        client.subscribe('#')

    def add_thing(self, payload):
        message = json.loads(payload)
        name = message["name"]
        thing_type = message["type"]
        message_file = self.temp_dir + '/' + name + '-message.json'
        if os.path.isfile(self.temp_dir + '/' + name + '-message.json'):
          with open(message_file, 'r') as myfile:
            last_message = json.load(myfile);
          message_payload = json.dumps(last_message["data"]);
          self.client.publish(last_message["topic"], message_payload);            
                     
        self.status["things"][name] = message
        self.save_status()

    def remove_thing(self, payload):
        message = json.loads(payload)
        name = message["name"]
        thing_type = message["type"]
        if name in self.status["things"].keys():
            del self.status["things"][name]
            self.save_status()

    def on_message(self, client, userdata, message):
        print(message.topic)
        message.payload = message.payload.decode("utf-8")
        if message.topic == "monitor/stop":
            self.run = False
        elif message.topic == 'thing/join':
            self.add_thing(message.payload)         
        elif message.topic == 'thing/leave':
            self.remove_thing(message.payload)

    def listen(self):
        while self.run:
            time.sleep(0.500)
            self.client.loop()
        self.db.close()



