<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/12/17
 * Time: 8:58 PM
 */

namespace Pi;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

/**
 * Primary Application class
 */
class App {

  private static $instance;

  protected $install_location;
  protected $base_path = "/";

  /** @var UrlMatcher */
  protected $matcher;

  /** @var \Symfony\Component\EventDispatcher\EventDispatcher  */
  protected $dispatcher;

  /** @var \Symfony\Component\HttpKernel\HttpKernel  */
  protected $kernel;

  /** @var Request */
  protected $request;

  /**
   * Singleton Factory for Application
   * @return App
   */
  public static function instance() {
    if (static::$instance == NULL) {
      static::$instance = new static();
    }
    return static::$instance;
  }

  /**
   * App constructor.
   */
  public function __construct() {
    date_default_timezone_set('America/Los_Angeles');
    $this->install_location = dirname(__DIR__);
    $locator = new FileLocator($this->install_location);
    $loader = new YamlFileLoader($locator);
    $routes = $loader->load('routes.yml');
    $this->request = Request::createFromGlobals();
    // Create the url matcher.
    $this->matcher = new UrlMatcher($routes, new RequestContext());

    $this->dispatcher = new EventDispatcher();

    $this->dispatcher->addSubscriber(new RouterListener($this->matcher, new RequestStack()));

    $controllerResolver = new ControllerResolver();
    $argumentResolver = new ArgumentResolver();

    $this->kernel = new HttpKernel($this->dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

  }

  public function handle() {
    $response = $this->kernel->handle($this->request);
    $response->send();
    $this->kernel->terminate($this->request, $response);
  }

}