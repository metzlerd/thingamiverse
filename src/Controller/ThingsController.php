<?php

namespace Pi\Controller;

use Pi\Model\Thingamiverse;
use Pi\Things\ThingInterface;
use Pi\View\Page;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \stdClass;
use \Exception;

class ThingsController {

  protected $ajaxResponse;

  /** @var Thingamiverse */
  protected $things;

  public function __construct() {
    $this->ajaxResponse = new \stdClass();
    $this->ajaxResponse->success = TRUE;
    $this->things = Thingamiverse::service();
  }

  /**
   * Return the ajax repsonse for the method.
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  protected function getAjaxResponse() {
    return new JsonResponse($this->ajaxResponse);
  }

  /**
   * Main page
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function page() {

    $page = new Page();
    return new Response(
      $page->show()
    );
  }

  /**
   * Add a jquery command.
   */
  public function addJQueryCommand($selector, $command, $args=[]) {
    $args = (array)$args;
    if (!isset($this->ajaxResponse->jQuery)) {
      $this->ajaxResponse->jQuery = [];
    }
    $data = new \stdClass();
    $data->$selector = new \stdClass();
    $data->$selector->$command = $args;
    $this->ajaxResponse->jQuery[] = $data;
  }

  /**
   * Action the things.
   * @param $name
   * @param $action
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function thing($name, $action) {
    /** @var ThingInterface $thing */
    $thing = $this->things->getThing($name);
    switch ($action) {
      case "view":
        $content = $thing->view($_GET);
        $this->addJQueryCommand('#thing', 'html', $content);
        break;
      default:
        $thing->processAction($action, $_REQUEST);
    }
    return $this->getAjaxResponse();
  }

  /**
   * Get the status.
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function status() {
    $this->addJQueryCommand('#the-things', 'html', $this->things->statusView());
    $this->addJQueryCommand('body', 'thingStatus', $this->things->status);
    return $this->getAjaxResponse();
  }

  public function api(Request $request, $action ) {
    header('Access-Control-Allow-Origin: *');
    $response_status = 200;
    $response = new \stdClass();
    $response->success = TRUE;
    $post_data = file_get_contents('php://input');
    if ($post_data) {
      $data = json_decode($post_data);
    }
    else {
      $data = new stdClass();
    }
    try {

      switch (TRUE) {
        // Determine the
        case ($_SERVER['REQUEST_METHOD'] == 'POST' && $action == 'send' ):
          $response->success = $this->things->publish($data);
          break;
        case ($_SERVER['REQUEST_METHOD'] == 'GET' && $action == 'status'):
          $response = $this->things->readStatus();
          break;
        case ($_SERVER['REQUEST_METHOD'] == 'GET' && $action):
          $device = $action;
          $response = $this->things->lastMessage($device);
          break;

      }
    }
    catch (Exception $e) {
      $response->error = $e->getMessage();
      $response_status = 500;
    }
    $response = new JsonResponse($response, $response_status);
    $response->headers->set('Access-Control-Allow-Headers', 'Content-type');
    $response->headers->set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
    return $response;
  }


}