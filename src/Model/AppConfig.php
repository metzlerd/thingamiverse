<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/20/17
 * Time: 10:17 PM
 */

namespace Pi\Model;


class AppConfig {

  /** @var \stdClass */
  private $config;

  /** @var  static */
  static $instance;

  public function __construct() {
    $file_name = dirname(dirname(__DIR__)) . '/config/config.json';
    $config = file_get_contents($file_name);
    $this->config = json_decode($config);
  }

  /**
   * @return mixed|\stdClass
   */
  public function getConfig() {
    return $this->config;
  }

  static public function load() {
    if (static::$instance == NULL) {
      static::$instance = new static();
    }
    return static::$instance->getConfig();
  }

}