<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/17/17
 * Time: 9:27 PM
 */

namespace Pi\Model;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Symfony\Component\Yaml\Parser;

/**
 * MTQQ database manager class.
 * @package Pi\Model
 */
class DBManager {

  /** @var  DBManager */
  static protected $instance;

  private $db;
  private $config;

  public function __construct() {
    $this->config = new Configuration();
    $config = AppConfig::load();
    $this->db = DriverManager::getConnection(get_object_vars($config->mqtt_database), $this->config);
    if ($this->db) {
      $this->db->setFetchMode( \PDO::FETCH_OBJ );
    }
  }

  public function publishMessage($topic, $data=[]) {
    $parms['topic'] = $topic;
    $parms['action'] = 'p';
    $parms['data'] = json_encode($data);
    $this->db->executeUpdate(
      "INSERT INTO messages(action, topic, data) values (:action, :topic, :data)",
      $parms
    );
  }


  public function getMessages($topic) {
    $topic = str_replace('#', '%', $topic);
    $mesages = [];
    $result = $this->db->executeQuery(
      "select * from messages where topic like :topic",
      ['topic' => $topic]
    );
    foreach ($result->fetchAll() as $row) {
      $msg = new Message($row->topic);
      $msg->recorded = $row->recorded ? new \DateTime($row->recorded) : NULL;
      $msg->action = $row->action;
      $msg->data = $row->data ? json_decode($row->data) : new \stdClass();
      $mesages[] = $msg;
    }
    return $mesages;
  }

  public function deleteTopic($topic) {
    $topic = str_replace('#', '%', $topic);
    $this->db->executeUpdate(
      "delete from messages where topic like :topic",
      ['topic' => $topic]
      );
  }

  /**
   * Singleton factory.
   * @return \Pi\Model\DBManager
   */
  static public function service() {
    if (static::$instance == NULL) {
      static::$instance = new DBManager();
    }
    return static::$instance;
  }
}