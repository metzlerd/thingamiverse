<?php

namespace Pi\Model;

/**
 * MQTT Message
 */
class Message {

  public $mid;

  public $topic;

  public $action;

  public $data;

  /**
   * @var \DateTime
   */
  public $recorded;

  public function __construct($topic) {
    $this->topic = $topic;
    $this->data = new \stdClass();
  }

}
