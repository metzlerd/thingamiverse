<?php

namespace Pi\Model;
use Forena\Data\DataService;
use Pi\Things\ClaireLights;
use Pi\Things\LolinV3;
use Pi\View\Template;

/**
 * Class Thingamiverse
 * @package Pi\Model
 */
class Thingamiverse {

  const STATUS_FILE = 'things.json';

  /** @var  string */
  private $host;

  /**
   * @var string
   *   Data directory
   */
  private $dir;

  public $status;

  /** @var  static */
  static $instance;

  /**
   * Thingamiverse constructor.
   */
  public function __construct() {
    $conf = AppConfig::load();
    $this->host = $conf->mqtt->host;
    $this->dir = dirname(dirname(__DIR__)) . '/things';
    $this->status_file = $this->dir . '/' . static::STATUS_FILE;
    $this->readStatus();
  }

  /**
   * Read the status from the file system.
   */
  public function readStatus() {
    if (file_exists($this->status_file)) {
      $json = file_get_contents($this->status_file);
      $this->status = json_decode($json);
    }
    else {
      $this->status = new \stdClass();
    }
    return $this->status;
  }


  /**
   * Publish a message to a thing.
   * @param \Pi\Model\Message $message
   *   Message to publish.
   */
  public function publish($message) {
    // Create message
    $host = $this->host;
    $topic = $message->topic ?? "";
    $data = $message->data ?? new \stdClass();
    $json =  json_encode($data);

    if (strpos($topic, '/')) {
      $device = (explode('/', $topic, 2))[0];
    } else {
      $device = $topic;
    }

    $message_json = json_encode($message);
    $message_file = $this->dir . '/' . "$device-message.json";
    file_put_contents($message_file, $message_json);

    // Create a file for the payload.
    $file_name = tempnam($this->dir, 'pub');
    file_put_contents($file_name, $json);

    // Send the message
    $result = 0;
    $output = [ ];
    exec("mosquitto_pub -t '$topic' -h $host -f $file_name", $output, $result );
    unlink($file_name);
    return $result;
  }

  public function lastMessage($device) {
    $message_file = $this->dir . '/' . "$device-message.json";
    if (file_exists($message_file)) {
      $json = file_get_contents($message_file);
      $message = json_decode($json);
    }
    else {
      $message = new \stdClass();
    }
    return $message;
  }

  public function statusView() {
    DataService::service()->addContext($this->status->things, 'things');
    return Template::create('thingstatus.html')->show();
  }

  /**
   * Static Facotry method.
   * @return static
   */
  public static function service() {
    if (static::$instance == NULL) {
      static::$instance = new static();
    }
    return static::$instance;
  }

  /**
   * Get a thing assuming that it is active.
   */
  public function getThing($name) {
    $things = $this->status->things;
    $thing = NULL;
    if (isset($things->$name)) {
      $tdef = $things->$name;
      $ttype = $tdef->type;
      $tname = $tdef->name;
      switch ($ttype) {
        case "clairelights":
          $thing = new ClaireLights($this, $tname);
          break;
        case "lolinv3":
          $thing = new LolinV3($this, $tname);
          break;
      }
    }
    return $thing;
  }

}