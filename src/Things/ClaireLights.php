<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 11/5/17
 * Time: 5:45 PM
 */

namespace Pi\Things;


use Forena\Data\DataService;
use Pi\Model\Message;
use Pi\View\Template;

class ClaireLights extends ThingBase {

  public $interval;
  public $stripe;

  /**
   * @param array $parms
   *   Determine action.
   * @return string
   */
  public function view($parms=[]) {
    DataService::service()->addContext($this, $this->name);
    return Template::create('clairelights.html')->show();
  }

  public function off($parms) {
    $message = new Message($this->name . '/off');
    $this->publisher->publish($message);

  }

  public function lights($parms) {

    $message = new Message($this->name . '/lights');
    if (!empty($parms['effect'])){
      $message->data->effect=$parms['effect'];
    }

    if (!empty($parms['fill'])) {
      $message->data->fill=$parms['fill'];
    }

    if (!empty((int)$parms['speed'])) {
      $speed = (int)$parms['speed'];
      $message->data->interval = round(0.25-$speed/400.00,2);
    }

    if (!empty($parms['pixel'])) {
      $pixels = $parms['pixel'];
      $stripe = '';
      foreach ($pixels as $pixel) {
        $stripe .= $pixel;
      }
      if ($stripe) $message->data->stripe = $stripe;
    }
    $this->publisher->publish($message);
  }

  /**
   * @param $action
   * @param array $parms
   */
  public function processAction($action, $parms=[]) {
    switch ($action) {
      case 'lights':
        $this->lights($parms);
        break;
      case 'off':
        $this->off($parms);
        break;
    }
  }

}