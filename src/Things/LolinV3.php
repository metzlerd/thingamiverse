<?php

namespace Pi\Things;


use Forena\Data\DataService;
use Pi\Model\Message;
use Pi\View\Template;

class LolinV3 extends ThingBase {

  const TOPIC='lolinv3';

  protected $pins = [
    16 => ['pin' => 16 , 'label' => 'D0', 'function' => 'GPIO-16']
  ];

  protected $stripes=[];

  /**
   * Set a pin on the board.
   * @param int $pin
   * @param int $value
   */
  protected function message($pin, $value) {
    $msg = new Message($this->topic. '/pin');
    $msg->data->pin = (int)$pin;
    $msg->data->value = $value ? 1 : 0;
    $this->publisher->publish($msg);
  }

  /**
   * Set an IO pin's value.
   *
   * @param int $pin
   * @param int $value
   * @return $this
   */
  public function setPin($pin, $value) {
    if (isset($this->pins[$pin])) {
      $this->message($pin, $value);
    }
    return $this;
  }

  /**
   * Generate the view for the controller.
   * @return Template
   */
  public function view($parms = []) {
    DataService::service()->addContext($this->pins, 'lolin');
    return Template::create('lolinv3.html');
  }

  public function processAction($action, $parms = []) {
    // TODO: Implement processAction() method.
  }

  /**
   * Add a neopixel stripe to the message
   * @param $pin
   * @param $pixels
   * @param int $delay
   */
  public function neoPixelAdd($pin, $pixels, $delay=0) {
    $stripe = new \stdClass();
    $stripe->pin = (int)$pin;
    $stripe->pixels = $pixels;
    $stripe->delay = (int)$delay;
    $this->stripes[] = $stripe;
  }

  /**
   * Send a message for neopixel.
   * @param $count
   * @param int $delay
   */
  public function neoPixelSend($count, $delay=0) {
    $msg = new Message($this->topic. '/neopixel');
    $msg->data->count = (int)$count ? (int)$count : 1;
    $msg->data->delay = (int)$delay;
    $msg->data->stripes = $this->stripes;
    $this->publisher->publish($msg);
    $this->stripes=[];
  }

}