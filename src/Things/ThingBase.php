<?php


namespace Pi\Things;

use Pi\Model\Thingamiverse;

/**
 * Base class for things we want to control.
 */
abstract class ThingBase implements ThingInterface {

  public $name;

  /** @var  Thingamiverse */
  protected $publisher;

  public function __construct(Thingamiverse $publisher, $name) {
    if ($name) {
      $this->name = $name;
    }
    $this->publisher = $publisher;
  }
}
