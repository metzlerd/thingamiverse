<?php

namespace Pi\Things;


interface ThingInterface {

  /**
   * @param $action
   * @param array $parms
   */
  public function processAction($action, $parms=[]);

  /**
   * @param array $parms
   * @return string
   */
  public function view($parms=[]);

}