<?php

namespace Pi\View;
use Forena\Data\DataService;
use Forena\Render\HTML\Element;

/**
 * Main HTML page template
 */
class Page {

  protected $dir = '';
  protected $template;

  public function __construct() {
    $info = new \stdClass();
    $info->timestamp = time();
    DataService::service()->addContext($info, 'page');
    $this->dir = dirname(dirname(__DIR__)) . '/templates/';
    $html = Element::create('html');
    $this->template = \Forena\Template\HTMLTemplate::load($this->dir . "main.html", $html);
  }

  public function show() {
    return $this->template->show();
  }

}