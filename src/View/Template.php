<?php

namespace Pi\View;
use Forena\Render\HTML\Element;

/**
 * Template loader for Forena
 */
class Template {

  protected $dir = '';
  protected $template;

  /**
   * Template constructor.
   * @param $file
   * @param string $tag
   * @param array $attributes
   */
  public function __construct($file, $tag='div', $attributes=[]) {
    $this->dir = dirname(dirname(__DIR__)) . '/templates/';
    $element = Element::create($tag, $attributes);
    $this->template = \Forena\Template\HTMLTemplate::load($this->dir . $file, $element);
  }

  /**
   * Show method
   * @return mixed
   */
  public function show() {
    return $this->template->show();
  }

  /**
   * Factory method for syntactical sugar.
   * @param $file
   * @param string $tag
   * @param array $attributes
   * @return static
   */
  public static function create($file, $tag='div', $attributes=[]) {
    return new static($file, $tag, $attributes);
  }

}