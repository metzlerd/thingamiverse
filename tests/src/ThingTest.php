<?php
/**
 * Created by PhpStorm.
 * User: metzlerd
 * Date: 10/21/17
 * Time: 9:40 PM
 */

namespace Pi\Tests;


use Pi\Model\Message;
use Pi\Model\Thingamiverse;
use Pi\Things\ClaireLights;
use Pi\Things\Led;
use Pi\Things\LolinV3;

class ThingTest extends \PHPUnit_Framework_TestCase {

  /** @var  Thingamiverse */
  protected $things;

  /** @var  Message[] */
  protected $messages = [];

  public function setUp() {
    parent::setUp();
    $this->things = $this->getMockBuilder(Thingamiverse::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->things->method('publish')->willReturnCallback([$this, 'publish']);
  }

  public function publish(Message $message) {
    $this->messages[] = $message;
  }

  /**
   * @return \Pi\Model\Message[]
   */
  public function getMessages() {
    $messages = $this->messages;
    $this->messages=[];
    return $messages;
  }

  public function getMessage() {
    $messages = $this->getMessages();
    return reset($messages);
  }

  public function testClaireLights() {
    $c = new ClaireLights($this->things);
    $this->assertInstanceOf(ClaireLights::class, $c);

  }

}